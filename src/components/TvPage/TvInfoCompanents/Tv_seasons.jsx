import { React, useEffect, useState, useContext } from "react";
import Tv from "../Tv";
import "./tv-seasons.css";
import Carousel from "react-multi-carousel";
import IdContext from "../../../context2";

const Tv_seasons = ({ id }) => {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  const { seasonList } = useContext(IdContext);
  const { languageList } = useContext(IdContext);

  const [seasons, setSeasons] = useState([]);
  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${id}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setSeasons(data.seasons);
      });
    console.log(seasons);
  }, []);

  return (
    <div className="season-con">
      {seasons.length > 0 ? (
        <div>
          <h3 className="season-title">Seasons</h3>
          <Carousel
            itemClass="carousel-item-padding-10-px"
            responsive={responsive}
          >
            {seasons.length > 0 &&
              seasons.map((season) => {
                return (
                  <Tv
                    id={id}
                    key={season.id}
                    qwe="tv_season"
                    name={season.name}
                    overview={season.overview}
                    poster_path={season.poster_path}
                    season_number={season.season_number}
                  />
                );
              })}
          </Carousel>
        </div>
      ) : null}
    </div>
  );
};

export default Tv_seasons;
