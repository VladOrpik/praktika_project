import { React, useState, useEffect, useContext } from "react";
import Tv from "../Tv";
import "./tv-rec.css";
import IdContext from "../../../context2";
const Tv_rec = ({ id }) => {
  const [rec, setRec] = useState([]);
  const { languageList } = useContext(IdContext);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${id}/recommendations?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}&page=1`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setRec(data.results);
      });
  }, []);
  return (
    <div>
      {rec.length > 0 ? (
        <div>
          <h3 className="rec-title">recommendations show</h3>
          <div className=" rec-container scroll-wrapper" id="asd">
            {rec.length > 0 &&
              rec.map((recom) => {
                return (
                  <div className="zxc">
                    <Tv qwe="tv" {...recom} />
                  </div>
                );
              })}
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default Tv_rec;
