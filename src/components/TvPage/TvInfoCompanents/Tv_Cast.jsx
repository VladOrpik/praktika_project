import { React, useEffect, useState } from "react";
import People from "../../HomePage/People";
import "./tv-cast.css";
const Tv_Cast = ({ id }) => {
  const [cast, setCast] = useState([]);
  console.log("asdasd");
  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${id}/credits?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=en-US`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCast(data.cast);
      });
  }, []);

  return (
    <div className="scroll-wrapper" id="asd">
      {cast.length > 0 &&
        cast.map((actors) => {
          return (
            <div className="zxc">
              <People {...actors} />
            </div>
          );
        })}
    </div>
  );
};

export default Tv_Cast;
