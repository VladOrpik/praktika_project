import { React, useState, useEffect, useContext } from "react";
import Tv_Image from "../Tv_Image";
import Header from "../../../HomePage/Header";
import IdContext from "../../../../context2";
import SeasonsCast from "./SeasonCast";
import Tv_SeasonsVideo from "./Tv_SeasonsVideo";
import "./season-cast.css";
import { observer } from "mobx-react-lite";
import Seasons_Episod from "./Seasons_Episod";

const Seasons_info = (props) => {
  const [seasonsInfo, setSeasonsInfo] = useState();
  const { languageList } = useContext(IdContext);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${props.$stateParams.id}/season/${props.$stateParams.season_number}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setSeasonsInfo(data);
      });
  }, []);
  return (
    <div className="seasonsInfo-con">
      <Header />
      <Tv_Image qwe={"tv_season"} {...seasonsInfo} />
      <div>
        {seasonsInfo && seasonsInfo.season_number ? (
          <SeasonsCast season_number={seasonsInfo.season_number} />
        ) : null}
      </div>
      <div>
        {seasonsInfo && seasonsInfo.season_number ? (
          <Tv_SeasonsVideo season_number={seasonsInfo.season_number} />
        ) : null}
      </div>
      <div>
        {seasonsInfo && seasonsInfo.season_number ? (
          <Seasons_Episod season_number={seasonsInfo.season_number} />
        ) : (
          "123"
        )}
      </div>
    </div>
  );
};

export default observer(Seasons_info);
