import { React, useState, useEffect, useContext } from "react";
import IdContext from "../../../../context2";
import Tv from "../../Tv";
import "./season-cast.css";
const Seasons_Episod = ({ season_number }) => {
  const [episod, setEpisod] = useState([]);
  const { seasonList } = useContext(IdContext);
  const { languageList } = useContext(IdContext);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${localStorage.getItem(
        "tv_id"
      )}/season/${season_number}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${
        languageList.languageItem
      }`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setEpisod(data.episodes);
      });
  }, []);

  return (
    <div>
      <h3 className="episode-title">Episode</h3>
      <div className="episod-con">
        {episod.length > 0 &&
          episod.map((ep) => {
            return (
              <Tv
                qwe="tv_episod"
                id={seasonList.seasonItem}
                name={ep.name}
                still_path={ep.still_path}
                overview={ep.overview}
                episode_number={ep.episode_number}
                season_number={ep.season_number}
              />
            );
          })}
      </div>
    </div>
  );
};
export default Seasons_Episod;
