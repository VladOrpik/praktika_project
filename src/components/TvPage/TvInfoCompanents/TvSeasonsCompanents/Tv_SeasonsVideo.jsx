import { React, useState, useEffect, useContext } from "react";
import Carousel from "react-multi-carousel";
import Card from "react-multi-carousel";
import play_icon from "../../../../img/play-button.png";
import "../tv-video.css";
import IdContext from "../../../../context2";

const Tv_SeasonsVideo = ({ season_number }) => {
  const [currentTrailer, setCurrentTrailer] = useState();
  const [videos, setVideos] = useState([]);
  const [videosMain, setVideosMain] = useState([]);
  const { seasonList } = useContext(IdContext);
  const { languageList } = useContext(IdContext);

  const VIDEO_API = `https://api.themoviedb.org/3/tv/${localStorage.getItem(
    "tv_id"
  )}/season/${season_number}/videos?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${
    languageList.languageItem
  }`;

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  useEffect(() => {
    fetch(VIDEO_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setVideos(data.results);
        setVideosMain(data.results[0]);
        setCurrentTrailer(data.results[0].key);
      });
  }, []);

  return (
    <div>
      <div className="main-video">
        <iframe
          src={`https://www.youtube.com/embed/${currentTrailer}?autoplay=1&mute=1`}
          frameBorder="0"
          allow="autoplay; encrypted-media"
          allowFullScreen
          title="video"
          height="600px"
          width="100%"
        />
      </div>
      {videos.length > 2 ? (
        <div>
          <h3 className="oth">Other Trailer</h3>
          <Carousel
            itemClass="carousel-item-padding-40-px"
            responsive={responsive}
          >
            {videos.length > 2 &&
              videos.map((video) => {
                return (
                  <div key={video.id}>
                    <a onClick={() => setCurrentTrailer(video.key)}>
                      <img
                        className="add"
                        src={`https://i.ytimg.com/vi/${video.key}/maxresdefault.jpg`}
                        alt="trailer"
                      />
                      <img className="player" src={play_icon} />
                    </a>
                  </div>
                );
              })}
          </Carousel>
        </div>
      ) : null}
    </div>
  );
};

export default Tv_SeasonsVideo;
