import { React, useState, useEffect, useContext } from "react";
import People from "../../../HomePage/People";
import IdContext from "../../../../context2";
import "./season-cast.css";
import "../../../HomePage/peopleInfo.css";
const SeasonsCast = ({ season_number }) => {
  const [seasonCast, setSeasonCast] = useState([]);
  const [newseasonCast, setNewSeasonCast] = useState([]);
  const { seasonList } = useContext(IdContext);
  const [use, SetUse] = useState(false);
  const { languageList } = useContext(IdContext);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${localStorage.getItem(
        "tv_id"
      )}/season/${season_number}/credits?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${
        languageList.languageItem
      }`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setSeasonCast(data.cast);
      });
  }, []);

  useEffect(() => {
    setNewSeasonCast(seasonCast.slice(0, 8));
  }, [seasonCast]);

  function ShowAll() {
    setNewSeasonCast(seasonCast);
    SetUse(!use);
  }
  function HideAll() {
    setNewSeasonCast(seasonCast.slice(0, 8));
    SetUse(!use);
  }

  return (
    <div>
      <h3 className="cast-title">Cast</h3>
      <div className="seasonCast-con">
        {newseasonCast.length > 0 &&
          newseasonCast.map((seas) => {
            return <People {...seas} />;
          })}
        <button
          onClick={() => ShowAll()}
          type="button"
          className={`btn btn-primary ${use ? "qaz" : "movieButton"}`}
        >
          Show all
        </button>
        <button
          onClick={() => HideAll()}
          type="button"
          className={`btn btn-danger ${use ? "movieButton" : "qaz"}`}
        >
          Hide
        </button>
      </div>
    </div>
  );
};
export default SeasonsCast;
