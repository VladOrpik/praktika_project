import React from "react";
import "../../HomePage/review.css";
import none_img from "../../../img/no-photo.png";

const Tv_rev = ({ author, content, created_at, author_details }) => {
  const IMG_API = "https://image.tmdb.org/t/p/w500";

  return (
    <div className="rev-container">
      <div className="review-content">
        <div className="review-person">
          <h3 className="review-author">{author}</h3>
          {author_details.avatar_path ? (
            <img
              className="review-img"
              src={IMG_API + author_details.avatar_path}
            />
          ) : (
            <img className="review-img" src={none_img} />
          )}

          {author_details.rating ? (
            <h5 className="rate"> Rating {author_details.rating}</h5>
          ) : null}
        </div>
        <div className="review-text">
          <p align="justify" className="review-text">
            {content}
          </p>
          <hr />
        </div>
      </div>
    </div>
  );
};

export default Tv_rev;
