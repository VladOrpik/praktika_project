import { React, useEffect, useState, useContext } from "react";
import { Heart } from "react-feather";
import { List } from "react-feather";
import "./tv-image.css";
import "../../HomePage/movie-info.css";
import IdContext from "../../../context2";

const Tv_Image = ({
  backdrop_path,
  original_name,
  original_language,
  tagline,
  popularity,
  status,
  genres,
  name,
  air_date,
  overview,
  poster_path,
  still_path,
  vote_overage,
  vote_count,
  season_number,
  id,
}) => {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const [rate, setRate] = useState();
  const [favorite, setFavorite] = useState();
  const [watchlist, setWatchlist] = useState();
  const [isActive, setIsActive] = useState(false);
  const { languageList } = useContext(IdContext);
  const [isUse, setIsUse] = useState(false);

  useEffect(() => {
    GiveRate();
  }, []);

  useEffect(() => {}, [rate, favorite, watchlist]);

  async function Rate(rating) {
    const result = await fetch(
      `https://api.themoviedb.org/3/tv/${id}/rating?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({ value: rating }),
      }
    );
    const req = await result.json();
    setTimeout(() => GiveRate(), 1000);
    return req;
  }

  async function GiveRate() {
    const result = await fetch(
      `https://api.themoviedb.org/3/tv/${localStorage.getItem(
        "tv_id"
      )}/account_states?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`
    );
    const req = await result.json();
    setRate(req.rated.value);
    setFavorite(req.favorite);
    setWatchlist(req.watchlist);
    return req;
  }

  async function DeleteRate() {
    const result = await fetch(
      `https://api.themoviedb.org/3/tv/${localStorage.getItem(
        "tv_id"
      )}/rating?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "DELETE",
      }
    );
    const req = await result.json();
    setRate();
    setTimeout(() => GiveRate(), 1000);
    return req;
  }

  async function addList() {
    const result = await fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/favorite?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          media_type: "tv",
          media_id: localStorage.getItem("tv_id"),
          favorite: true,
        }),
      }
    );
    const req = await result.json();
    alert("added in favorite list");
    setIsUse(!isUse);

    setTimeout(() => GiveRate(), 1000);
    return req;
  }

  async function DeleteFavorite() {
    const result = await fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/favorite?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          media_type: "tv",
          media_id: localStorage.getItem("tv_id"),
          favorite: false,
        }),
      }
    );
    const req = await result.json();
    setIsUse(!isUse);
    setFavorite();
    alert("deleted");
    setTimeout(() => GiveRate(), 1000);
    return req;
  }

  async function addWatchList() {
    const result = await fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/watchlist?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          media_type: "tv",
          media_id: localStorage.getItem("tv_id"),
          watchlist: true,
        }),
      }
    );
    const req = await result.json();
    setIsUse(!isUse);
    setTimeout(() => GiveRate(), 1000);
    alert("added in watch list");
    return req;
  }

  function show() {
    setIsActive(!isActive);
  }
  function hide() {
    setIsActive(!isActive);
    console.log(rate);
    alert("оценка поставлена");
  }

  const IMG_API = "https://image.tmdb.org/t/p/w1280";

  return (
    <div className="main-con">
      <div className="info-container2">
        {poster_path || still_path ? (
          <div
            className="info-back"
            style={{
              backgroundImage: `url(${
                poster_path ? IMG_API + poster_path : IMG_API + still_path
              })`,
            }}
          >
            <div className="shadow">
              <div className="block-info">
                {name ? <h2 className="movie_title">{name}</h2> : null}
                {popularity ? (
                  <span className="movie-time">Popularity{popularity}</span>
                ) : null}
                {air_date ? (
                  <span className="movie-time">Air Date {air_date}</span>
                ) : null}
                {original_language ? (
                  <span className="movie-budget">
                    {`Original language ${original_language}`}{" "}
                  </span>
                ) : null}
                {tagline ? (
                  <span className="movie-text">{`${tagline}`} </span>
                ) : null}
                {vote_overage ? (
                  <h5 className="movie-text">{`${vote_overage}`} </h5>
                ) : null}
                {vote_count ? (
                  <h5 className="movie-text">{` vote_count ${vote_count}`} </h5>
                ) : null}
                {rate ? (
                  <div>
                    <h3 className="btn btn-primary">{rate}</h3>
                    <button
                      className="btn btn-primary"
                      onClick={() => {
                        DeleteRate();
                      }}
                    >
                      DELETE RATE
                    </button>
                  </div>
                ) : (
                  <div className="rate-container">
                    {arr.map((rating) => (
                      <button
                        onClick={() => {
                          hide();
                          Rate(rating);
                        }}
                        className={`${isActive ? "movie__rate-item" : "hide"}`}
                      >
                        {rating}
                      </button>
                    ))}
                    <button
                      onClick={() => show()}
                      className={`${isActive ? "hide" : "btn btn-primary"}`}
                    >
                      Rate the movie
                    </button>
                  </div>
                )}
                {favorite ? (
                  <div>
                    <div className={`${isUse ? "svg-con" : "no-hov"}`}>
                      <Heart
                        className="no-hov"
                        fill="red"
                        size={45}
                        color="red"
                        onClick={() => DeleteFavorite()}
                      />
                      <h4 className="icon-name">favorite</h4>
                    </div>
                  </div>
                ) : (
                  <div className={`${isUse ? "no-hov" : "svg-con"}`}>
                    <Heart
                      className="hov"
                      color="red"
                      size={45}
                      onClick={() => addList()}
                    />
                    <h4 className="icon-name">favorite</h4>
                  </div>
                )}
                <div className="list-con">
                  {watchlist ? (
                    <div>
                      <List
                        color="yellow"
                        className="hovWatch"
                        size={45}
                        onClick={() => addWatchList()}
                      />
                    </div>
                  ) : (
                    <div>
                      <List
                        className="hovWatch"
                        size={45}
                        onClick={() => addWatchList()}
                      />
                    </div>
                  )}
                </div>

                {/* {season_number ? (
                  <h5 className="movie-text">
                    {`  season_number ${season_number}`}{" "}
                  </h5>
                ) : null} */}
              </div>
            </div>
          </div>
        ) : (
          <div
          // className="info-back"
          // style={{
          //   backgroundImage: `url(${IMG_API + backdrop_path})`,
          // }}
          >
            {/* <div className="shadow">
              <div className="block-info">
                {name ? <h2 className="movie_title">{name}</h2> : null}
                {popularity ? (
                  <span className="movie-time">Popularity{popularity}</span>
                ) : null}
                {air_date ? (
                  <span className="movie-time">Air Date {air_date}</span>
                ) : null}
                {original_language ? (
                  <span className="movie-budget">
                    {`Original language ${original_language}`}{" "}
                  </span>
                ) : null}
                {tagline ? (
                  <span className="movie-text">{`${tagline}`} </span>
                ) : null}
              </div>
            </div> */}
            123
          </div>
        )}
      </div>
    </div>
  );
};

export default Tv_Image;
