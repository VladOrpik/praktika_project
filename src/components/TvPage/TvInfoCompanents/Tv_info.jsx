import { React, useState, useEffect, useContext } from "react";
import "./tv-info.css";
import Tv_Image from "./Tv_Image";
import Tv_Created from "./Tv_Created";
import People from "../../HomePage/People";
import Tv_Cast from "./Tv_Cast";
import TvFacts from "./TvFacts";
import TvVideo from "./TvVideo";
import Header from "../../HomePage/Header.jsx";
import Tv_overwiev from "./Tv_overwiev";
import Tv_seasons from "./Tv_seasons";
import Tv_rec from "./Tv_rec";
import Tv_Reviews from "./Tv_Reviews";
import IdContext from "../../../context2";
import { observer } from "mobx-react-lite";

const Tv_info = (props) => {
  const [mainInfo, setMainInfo] = useState();
  const { languageList } = useContext(IdContext);
  const { seasonList } = useContext(IdContext);
  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${props.$stateParams.id}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setMainInfo(data);
        seasonList.seasonItem = data.id;
        localStorage.setItem("tv_id", data.id);
        console.log(seasonList.seasonItem);
      });
  }, []);

  return (
    <div className="tv-main">
      <Header />
      <Tv_Image {...mainInfo} />
      <h2 className="actors-title">The cast of the series</h2>
      {mainInfo && mainInfo.id ? (
        <div>
          <div className="tvInfo-con">
            <div className="actors">
              <Tv_Cast id={mainInfo.id} />
            </div>
            <div>
              <TvFacts {...mainInfo} />
            </div>
          </div>
          <div>
            <TvVideo id={mainInfo.id} />
          </div>
          <div>
            <Tv_overwiev {...mainInfo} />
          </div>
          <div>
            <Tv_seasons {...mainInfo} />
          </div>
          <div>
            <Tv_rec id={mainInfo.id} />
          </div>
          <div>
            <Tv_Reviews id={mainInfo.id} />
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default observer(Tv_info);
