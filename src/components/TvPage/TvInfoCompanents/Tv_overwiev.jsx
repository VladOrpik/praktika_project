import { React, useEffect, useState, useContext } from "react";
import IdContext from "../../../context2";
const Tv_overwiev = ({ overview, id }) => {
  const [companies, setCompanies] = useState([]);
  const { languageList } = useContext(IdContext);
  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${id}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCompanies(data.production_companies);
      });
  }, []);

  return (
    <div className="movie__par">
      <div className="movie-overview">
        <h3 className="overview-title">overview</h3>
        <p align="justify" className="overview-text">
          {overview}
        </p>
      </div>
      <div className="prod-info">
        <h3 className="production-title">Production Companies</h3>
        {companies.length > 0 &&
          companies.map((prod) => {
            return (
              <ul key={prod.id} className="prod-list">
                <li className="prod-text">{prod.name}</li>
              </ul>
            );
          })}
      </div>
    </div>
  );
};

export default Tv_overwiev;
