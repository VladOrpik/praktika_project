import { React, useState, useEffect, useContext } from "react";
import People from "../../../HomePage/People";
import IdContext from "../../../../context2";
import "./tv-episode.css";

const GuestStars = ({ season_number, episode_number }) => {
  const [guest, setGuest] = useState([]);
  const { languageList } = useContext(IdContext);

  const { seasonList } = useContext(IdContext);
  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${localStorage.getItem(
        "tv_id"
      )}/season/${season_number}/episode/${episode_number}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${
        languageList.languageItem
      }`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setGuest(data.guest_stars);
      });
  }, []);

  return (
    <div>
      {guest.length > 0 ? (
        <div>
          <h3>GuestStars</h3>
          <div className="stars-con">
            {guest.length > 3 &&
              guest.map((stars) => {
                return <People {...stars} />;
              })}
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default GuestStars;
