import { React, useState, useEffect, useContext } from "react";
import Tv_Image from "../Tv_Image";
import IdContext from "../../../../context2";
import "./tv-episode.css";
import GuestStars from "./GuesStars";
import EpisodeVideo from "./EpisodeVideo";
import Header from "../../../HomePage/Header";

const TvEpisods = (props) => {
  const [episodInfo, setEpisodInfo] = useState();
  const { languageList } = useContext(IdContext);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${localStorage.getItem(
        "tv_id"
      )}/season/${props.$stateParams.season_number}/episode/${
        props.$stateParams.episode_number
      }?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${
        languageList.languageItem
      }`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setEpisodInfo(data);
      });
  }, []);
  return (
    <div className="episode-container">
      <Header />
      <Tv_Image {...episodInfo} />
      {episodInfo ? (
        <GuestStars
          season_number={episodInfo.season_number}
          episode_number={episodInfo.episode_number}
        />
      ) : null}
      <div>
        {episodInfo ? (
          <EpisodeVideo
            season_number={episodInfo.season_number}
            episode_number={episodInfo.episode_number}
          />
        ) : null}
      </div>
    </div>
  );
};
export default TvEpisods;
