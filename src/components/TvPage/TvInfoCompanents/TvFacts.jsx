import { React, useState, useEffect } from "react";
import facebool_icon from "../../../img/facebook_icon.png";
import imdb_icon from "../../../img/imdb_icon.png";
import twitter_icon from "../../../img/twitter_icon.png";
import instagram_icon from "../../../img/Instagram_icon.png";
import "./tv-facts.css";

const TvFacts = ({ id, original_name, status, type, original_language }) => {
  const [external, setExternal] = useState({});
  const [keywords, setKeywords] = useState([]);

  useEffect(async () => {
    fetch(
      `https://api.themoviedb.org/3/tv/${id}/external_ids?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=en-US`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setExternal(data);
      })
      .then(
        await fetch(
          `https://api.themoviedb.org/3/tv/${id}/keywords?api_key=5006aee2ee6c14a6a59cfd3b10e5d068`
        )
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            setKeywords(data.results);
          })
      );
  }, []);

  return (
    <div>
      <ul className="external-con">
        <li>
          <a
            href={`https://www.imdb.com/name/${external.imdb_id}`}
            target="_blank"
          >
            <img width={"30px"} height={"30px"} src={imdb_icon} alt="img" />
          </a>
        </li>
        <li>
          <a
            href={`https://www.instagram.com/${external.instagram_id}`}
            target="_blank"
          >
            <img
              width={"30px"}
              height={"30px"}
              src={instagram_icon}
              alt="img"
            />
          </a>
        </li>
        <li>
          <a
            href={`https://www.twitter.com/${external.twitter_id}`}
            target="_blank"
          >
            <img width={"30px"} height={"30px"} src={twitter_icon} alt="img" />
          </a>
        </li>
        <li>
          <a
            href={`https://www.facebook.com/${external.facebook_id}`}
            target="_blank"
          >
            <img width={"30px"} height={"30px"} src={facebool_icon} alt="img" />
          </a>
        </li>
      </ul>
      <h2 className="facts-title2">Facts</h2>
      <div className="facts">
        <p className="facts-title">original name: {original_name}</p>
        <p className="facts-title">status: {status}</p>
        <p className="facts-title">type: {type}</p>
        <p className="facts-title">original_language: {original_language}</p>
        <h2 className="facts-title2">keywords:</h2>
        <ul className="key-con">
          {keywords.length > 0 &&
            keywords.map((keyw) => {
              return <li className="keywords"> {keyw.name}</li>;
            })}
        </ul>
      </div>
    </div>
  );
};

export default TvFacts;
