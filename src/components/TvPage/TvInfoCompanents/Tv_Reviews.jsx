import { React, useEffect, useState, useContext } from "react";
import Tv_rev from "./Tv_rev";
import "./tv-info.css";
import IdContext from "../../../context2";

const Tv_Reviews = ({ id }) => {
  const [rewievs, setRewiesv] = useState([]);
  const { languageList } = useContext(IdContext);

  useEffect(() => {
    fetch(
      `
      https://api.themoviedb.org/3/tv/${id}/reviews?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}&page=1`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setRewiesv(data.results);
      });
  }, []);

  return (
    <div>
      {rewievs.length > 0 ? (
        <div>
          <h3 className="reviws-title">Reviews</h3>
          {rewievs.length > 0 &&
            rewievs.map((rew) => {
              return <Tv_rev key={rew.id} {...rew} />;
            })}
        </div>
      ) : null}
    </div>
  );
};

export default Tv_Reviews;
