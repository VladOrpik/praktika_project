import {React,useContext} from "react";
import './tv-show.css';
import { UISref, useRouter } from "@uirouter/react";
import {
  UIRouter,
  UIView,
  useSrefActive,
  pushStateLocationPlugin,
} from "@uirouter/react";
import none_img from "../../img/no-photo.png";
import IdContext from "../../context2";


const IMG_API = "https://image.tmdb.org/t/p/w500";

const Tv=({name,poster_path, overview, vote_average,id,tv_season,qwe,season_number,still_path,episode_number})=>{

  const activeClass = "active";
  const movieIdSref = useSrefActive(`tv`, null, activeClass);
  const asIdSref = useSrefActive(`tv_season`, null, activeClass);

    return(
        <div className="tv">
            <div className="tv-header">
            <UISref to={qwe} params={{ id,season_number,episode_number}}>
              {poster_path || still_path?
              <a>
              <img src={poster_path?IMG_API + poster_path : IMG_API + still_path } alt="title" />
            </a>
            :  <a>
            <img height={'450px'} src={none_img} alt="title" />
           
          </a>
          }
     
        </UISref>
        
         </div>
         <div className="tv-info">
             <h6>{name}</h6>
             {season_number?  <h6> Season {season_number}</h6> :null }
             {vote_average?
              <span>{vote_average}</span>:null}
            
         </div>
         {overview?
          <div className="tv-over">
          <h2>Overview:</h2>
          <p>{overview}</p>
      </div>:null}
        
        </div>
     
    )
 }



export default Tv;