import { React, useContext } from "react";
import "./movie.css";
import { observer } from "mobx-react-lite";
import { UISref, useRouter } from "@uirouter/react";
import none_img from "../../img/no-photo.png";
import {
  UIRouter,
  UIView,
  useSrefActive,
  pushStateLocationPlugin,
} from "@uirouter/react";
import IdContext from "../../context2";

const IMG_API = "https://image.tmdb.org/t/p/w1280";
function Movie({
  title,
  poster_path,
  overview,
  vote_average,
  id,
  rating,
  name,
  qwe,
}) {
  const { movieList } = useContext(IdContext);
  const { profilemovieList } = useContext(IdContext);

  const router = useRouter();
  const activeClass = "active";
  const movieIdSref = useSrefActive(`movie`, null, activeClass);

  return (
    <div className="movie">
      <div className="movie-header">
        <UISref to="movie" params={{ id }}>
          {poster_path ? (
            <a>
              <img src={IMG_API + poster_path} alt="title" />
            </a>
          ) : (
            <a>
              <img height={"450px"} src={none_img} alt="title" />
            </a>
          )}
        </UISref>
      </div>
      <div className="movie-info">
        <h6>{title}</h6>
        {name ? <h6>{name}</h6> : null}
        <span>{vote_average}</span>
        {rating ? <h5>my rate{rating}</h5> : null}
      </div>
      {overview ? (
        <div className="movie-over">
          <h2>Overview:</h2>
          <p>{overview}</p>
        </div>
      ) : null}

      <UIView />
    </div>
  );
}

export default observer(Movie);
