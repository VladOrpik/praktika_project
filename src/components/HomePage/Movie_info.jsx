import { React, useEffect, useState, useRef } from "react";
import Carousel from "react-multi-carousel";
import Card from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "../HomePage/movie.css";
import "../HomePage/movie-info.css";
import play_icon from "../../img/play-button.png";
import Movie from "./Movie";
import "./movie.css";
import Review from "./review";
import People from "./People";
import IdContext from "../../context2";
import RateStore from "../../store/rateState";
import LanguageStore from "../../store/languageState";
import { useContext } from "react";
import { observer } from "mobx-react-lite";
import { Heart } from "react-feather";
import { List } from "react-feather";
import Header from "./Header";
import { Helmet } from "react-helmet";

const Movie_info = (props) => {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const [moviesInfo, setMoviesInfo] = useState({});
  const [production, setProduction] = useState([]);
  const [genres, setGenres] = useState([]);
  const [videos, setVideos] = useState([]);
  const [currentTrailer, setCurrentTrailer] = useState();
  const [videosMain, setVideosMain] = useState([]);
  const [recommend, setRecommend] = useState([]);
  const [review, setReview] = useState([]);
  const [reviewNew, setReviewNew] = useState([]);
  const [cast, setCast] = useState([]);
  const [castNew, setCastNew] = useState([]);
  const componentMounted = useRef(true);
  const [isActive, setIsActive] = useState(false);
  const [isUse, setIsUse] = useState(false);
  const [rate, setRate] = useState();
  const [rateById, setRateById] = useState();
  const [film, setFilm] = useState([]);

  const [favorite, setFavorite] = useState();
  const [watchlist, setWatchlist] = useState();

  const { rateList } = useContext(IdContext);
  const { languageList } = useContext(IdContext);

  const IMG_API = "https://image.tmdb.org/t/p/w1280";
  const PROD_API = "https://image.tmdb.org/t/p/w500";
  const MOVIEINFO_API = `https://api.themoviedb.org/3/movie/${props.$stateParams.id}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`;
  const VIDEO_API = `https://api.themoviedb.org/3/movie/${props.$stateParams.id}/videos?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`;
  const RECOMMEND_API = `https://api.themoviedb.org/3/movie/${props.$stateParams.id}/similar?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}&page=1`;
  const REVIEW_API = `https://api.themoviedb.org/3/movie/${props.$stateParams.id}/reviews?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}&page=1`;
  const CAST_API = `https://api.themoviedb.org/3/movie/${props.$stateParams.id}/credits?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`;

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  useEffect(() => {
    fetch(MOVIEINFO_API)
      .then((res) => res.json())
      .then((data) => {
        localStorage.setItem("imdb_id", data.imdb_id);
        setMoviesInfo(data);
        console.log(moviesInfo);
        setProduction(data.production_companies);
        setGenres(data.genres);
        console.log(languageList.languageItem);
      })
      .then(() =>
        fetch(
          `https://cors-anywhere.herokuapp.com/https://videocdn.tv/api/movies?api_token=Xo32OwwGexpAMGrRTHfzmCuZ6zHDdzoZ&imdb_id=${localStorage.getItem(
            "imdb_id"
          )}`,
          {
            headers: {
              "X-Requested-With": "get",
            },
          }
        )
          .then((res) => res.json())
          .then((data) => {
            setFilm(data.data);
            console.log(film);
          })
      );
  }, []);

  // useEffect(() => {
  //   fetch(
  //     `https://cors-anywhere.herokuapp.com/https://videocdn.tv/api/movies?api_token=Xo32OwwGexpAMGrRTHfzmCuZ6zHDdzoZ`,
  //     {
  //       headers: {
  //         "X-Requested-With": "get",
  //       },
  //     }
  //   )
  //     .then((res) => res.json())
  //     .then((respon) => {
  //       setFilm(respon.data);
  //       console.log(respon);
  //     });
  // }, []);

  useEffect(() => {
    fetch(VIDEO_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setVideos(data.results);
        setVideosMain(data.results[0]);
        setCurrentTrailer(data.results[0].key);
      });
  }, []);

  useEffect(() => {
    fetch(RECOMMEND_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setRecommend(data.results);
      });
  }, []);

  useEffect(() => {
    fetch(REVIEW_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (componentMounted.current) {
          setReview(data.results);
        }
      });
    return () => {
      componentMounted.current = false;
    };
  }, []);

  useEffect(() => {
    setReviewNew(review.slice(0, 2));
  }, [review]);

  useEffect(() => {
    fetch(CAST_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCast(data.cast);
      });
  }, []);
  useEffect(() => {
    setCastNew(cast.slice(0, 8));
  }, [cast]);

  useEffect(() => {
    GiveRate();
  }, []);
  console.log(moviesInfo.imdb_id);

  useEffect(() => {}, [rate, favorite, watchlist]);

  async function Rate(rating) {
    const result = await fetch(
      `https://api.themoviedb.org/3/movie/${
        props.$stateParams.id
      }/rating?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({ value: rating }),
      }
    );
    const req = await result.json();
    setTimeout(() => GiveRate(), 1000);
    return req;
  }

  async function GiveRate() {
    const result = await fetch(
      `https://api.themoviedb.org/3/movie/${
        props.$stateParams.id
      }/account_states?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`
    );
    const req = await result.json();
    setRate(req.rated.value);
    setFavorite(req.favorite);
    setWatchlist(req.watchlist);
    return req;
  }

  async function DeleteRate() {
    const result = await fetch(
      `https://api.themoviedb.org/3/movie/${
        props.$stateParams.id
      }/rating?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "DELETE",
      }
    );
    const req = await result.json();
    setRate();
    setTimeout(() => GiveRate(), 1000);
    return req;
  }

  function show() {
    setIsActive(!isActive);
  }
  function hide() {
    setIsActive(!isActive);
    console.log(rate);
    alert("оценка поставлена");
  }
  async function addList() {
    const result = await fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/favorite?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          media_type: "movie",
          media_id: props.$stateParams.id,
          favorite: true,
        }),
      }
    );
    const req = await result.json();
    alert("added in favorite list");
    setIsUse(!isUse);
    setTimeout(() => GiveRate(), 1000);
    return req;
  }

  async function addWatchList() {
    const result = await fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/watchlist?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          media_type: "movie",
          media_id: props.$stateParams.id,
          watchlist: true,
        }),
      }
    );
    const req = await result.json();
    setIsUse(!isUse);
    setTimeout(() => GiveRate(), 1000);
    alert("added in watch list");
    return req;
  }
  async function removeWatchList() {
    const result = await fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/watchlist?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          media_type: "movie",
          media_id: props.$stateParams.id,
          watchlist: false,
        }),
      }
    );
    const req = await result.json();
    setIsUse(!isUse);
    setWatchlist();
    setTimeout(() => GiveRate(), 1000);
    alert("remove");
    return req;
  }

  async function DeleteFavorite() {
    const result = await fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/favorite?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          media_type: "movie",
          media_id: props.$stateParams.id,
          favorite: false,
        }),
      }
    );
    const req = await result.json();
    setIsUse(!isUse);
    setFavorite();
    alert("deleted");
    setTimeout(() => GiveRate(), 1000);
    return req;
  }

  return (
    <div className="qwe">
      <Header />
      <div className="info-container2">
        <div
          className="info-back"
          style={{
            backgroundImage: `url(${IMG_API + moviesInfo.backdrop_path})`,
          }}
        >
          <div className="shadow">
            <div className="block-info">
              <h2 className="movie_title">{moviesInfo.original_title}</h2>
              <span className="movie-time">
                {`Duration  ${moviesInfo.runtime} min`}{" "}
              </span>
              <span className="movie-budget">
                {`Original language ${moviesInfo.original_language}`}{" "}
              </span>
              <span className="movie-text">{`${moviesInfo.tagline}`} </span>
              {rate ? (
                <div>
                  <h3 className="btn btn-primary">{rate}</h3>
                  <button
                    className="btn btn-primary"
                    onClick={() => DeleteRate()}
                  >
                    DELETE RATE
                  </button>
                </div>
              ) : (
                <div className="rate-container">
                  {arr.map((rating) => (
                    <button
                      onClick={() => {
                        hide();
                        Rate(rating);
                      }}
                      className={`${isActive ? "movie__rate-item" : "hide"}`}
                    >
                      {rating}
                    </button>
                  ))}
                  <button
                    onClick={() => show()}
                    className={`${isActive ? "hide" : "btn btn-primary"}`}
                  >
                    Rate the movie
                  </button>
                </div>
              )}
              {favorite ? (
                <div>
                  <div className={`${isUse ? "svg-con" : "no-hov"}`}>
                    <Heart
                      className="no-hov"
                      fill="red"
                      size={45}
                      color="red"
                      onClick={() => DeleteFavorite()}
                    />
                    <h4 className="icon-name">favorite</h4>
                  </div>
                </div>
              ) : (
                <div className={`${isUse ? "no-hov" : "svg-con"}`}>
                  <Heart
                    className="hov"
                    color="red"
                    size={45}
                    onClick={() => addList()}
                  />
                  <h4 className="icon-name">favorite</h4>
                </div>
              )}
              {watchlist ? (
                <div>
                  <List
                    color="yellow"
                    size={45}
                    onClick={() => removeWatchList()}
                  />
                </div>
              ) : (
                <div>
                  <List size={45} onClick={() => addWatchList()} />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="gen__con">
        {genres.length > 0 &&
          genres.map((gen) => {
            return (
              <h4 key={gen.id} className="gen-title">
                {gen.name}
              </h4>
            );
          })}
      </div>
      <div className="main-video">
        <iframe
          src={`https://www.youtube.com/embed/${currentTrailer}?autoplay=1&mute=1`}
          frameBorder="0"
          allow="autoplay; encrypted-media"
          allowFullScreen
          title="video"
          height="600px"
          width="100%"
        />
      </div>

      {videos.length > 2 ? (
        <div>
          <h3 className="oth">Other Trailer</h3>
          <Carousel
            itemClass="carousel-item-padding-40-px"
            responsive={responsive}
          >
            {videos.length > 2 &&
              videos.map((video) => {
                return (
                  <div key={video.id}>
                    <a onClick={() => setCurrentTrailer(video.key)}>
                      <img
                        className="add"
                        src={`https://i.ytimg.com/vi/${video.key}/maxresdefault.jpg`}
                        alt="trailer"
                      />
                      <img className="player" src={play_icon} />
                    </a>
                    <span className="trailer_titile">{video.name}</span>
                  </div>
                );
              })}
          </Carousel>
        </div>
      ) : null}

      {film.length > 0 &&
        film.map((player) => {
          return (
            <div className="play-con">
              <iframe
                src={`${player.iframe_src}`}
                className="player-iframe"
              ></iframe>
            </div>
          );
        })}
      <div className="movie__par">
        <div className="movie-overview">
          <h3 className="overview-title">overview</h3>
          <p align="justify" className="overview-text">
            {moviesInfo.overview}
          </p>
        </div>
        <div className="prod-info">
          <h3 className="production-title">Production Companies</h3>
          {production.length > 0 &&
            production.map((prod) => {
              return (
                <ul key={prod.id} className="prod-list">
                  <li className="prod-text">{prod.name}</li>
                </ul>
              );
            })}
        </div>
      </div>
      <div className="cast__container">
        <h3 className="rev-title">cast</h3>
        {castNew.length > 0 &&
          castNew.map((people) => <People key={people.id} {...people} />)}
      </div>
      <div className="dfdfd">
        <h3 className="rec-title">recommended movies</h3>
        <Carousel
          itemClass="carousel-item-padding-10-px"
          responsive={responsive}
        >
          {recommend.length > 0 &&
            recommend.map((rec) => <Movie key={rec.id} {...rec} />)}
        </Carousel>
      </div>
      {reviewNew.length > 0 ? (
        <div className="review__container">
          <h3 className="rev-title">reviews</h3>
          {reviewNew.length > 0 &&
            reviewNew.map((rev) => <Review key={rev.id} {...rev} />)}
        </div>
      ) : null}
    </div>
  );
};

export default observer(Movie_info);
