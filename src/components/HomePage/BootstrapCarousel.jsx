import { React, useState, useEffect, useContext } from "react";
import { Carousel } from "react-bootstrap";
import "./carousel.css";
import IdContext from "../../context2";
import { observer } from "mobx-react-lite";
import { UISref, useRouter } from "@uirouter/react";

const IMG_API = "https://image.tmdb.org/t/p/w500";
function BootstrapCarousel({ id }) {
  const [movieNow, setMovieNow] = useState([]);
  const [movieForSlider, setMovieForSlider] = useState([]);
  const { languageList } = useContext(IdContext);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/movie/now_playing?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}&page=1`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setMovieNow(data.results);
      });
  }, [languageList.languageItem]);

  useEffect(() => {
    setMovieForSlider(movieNow.slice(0, 5));
    console.log(languageList.languageItem);
  }, [movieNow]);

  return (
    <Carousel>
      {movieForSlider.map((now) => {
        return (
          <Carousel.Item key={now.id}>
            <UISref to="movie" params={{ id }}>
              <a>
                <img
                  className=" asd d-block  w-30"
                  src={IMG_API + now.poster_path}
                  alt="First slide"
                />
              </a>
            </UISref>
            <Carousel.Caption>
              <div className="cor-text">
                <h3 className="cor-title2">{now.title}</h3>
                <h2 className="cor-title">Playing now</h2>
              </div>
            </Carousel.Caption>
          </Carousel.Item>
        );
      })}
    </Carousel>
  );
}

export default observer(BootstrapCarousel);
