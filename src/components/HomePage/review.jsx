import React from "react";
import "./review.css";
import none_img from "../../img/no-photo.png";

const Review = ({ author, content, created_at, author_details }) => {
  const IMG_API = "https://image.tmdb.org/t/p/w500";

  let url = author_details.avatar_path;
  return (
    <div className="review-content">
      <div className="review-person">
        <h3 className="review-author">{author}</h3>
        {url ? (
          <img className="review-img" src={author_details.avatar_path} />
        ) : (
          <img className="review-img" src={none_img} />
        )}

        {author_details.rating ? (
          <h5 className="rate"> Rating {author_details.rating}</h5>
        ) : null}
      </div>
      <div className="review-text">
        <p align="justify" className="review-text">
          {content}
        </p>
        <hr />
      </div>
    </div>
  );
};

export default Review;
