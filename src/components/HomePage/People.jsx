import React from "react";
import "../HomePage/people.css";
import { useRouter, UISref, useSrefActive } from "@uirouter/react";
import none_img from "../../img/no-photo.png";
const People = ({
  name,
  profile_path,
  known_for_department,
  popularity,
  id,
}) => {
  const IMG_API = "https://image.tmdb.org/t/p/w1280";
  const activeClass = "active";
  const movieIdSref = useSrefActive(`people`, null, activeClass);
  return (
    <div className="people">
      <div className="people-header">
        <UISref to="people" params={{ id }}>
          {profile_path ? (
            <a>
              <img src={IMG_API + profile_path} alt="title" />
            </a>
          ) : (
            <a>
              <img height={"450px"} src={none_img} alt="title" />
            </a>
          )}
        </UISref>
      </div>
      <div className="people-info">
        <h6>{name}</h6>
        <span>{popularity}</span>
        <span>{known_for_department}</span>
      </div>
    </div>
  );
};

export default People;
