import { React, useContext, useState, useEffect } from "react";
import logo from "../../img/Logo.png";
import "./header.css";
import {
  UIRouter,
  UIView,
  useSrefActive,
  pushStateLocationPlugin,
} from "@uirouter/react";
import { observer } from "mobx-react-lite";
import IdContext from "../../context2";

const Header = () => {
  const { urlList } = useContext(IdContext);
  const { userDate } = useContext(IdContext);
  const activeClass = "active";
  const homeSref = useSrefActive("home", null, activeClass);
  const peopleSref = useSrefActive("peopleSearch", null, activeClass);
  const signInSref = useSrefActive("signIn", null, activeClass);
  const searchSref = useSrefActive("search", null, activeClass);
  const signUpSref = useSrefActive("signUp", null, activeClass);
  const tvShowSref = useSrefActive("tv_show", null, activeClass);
  const profileSref = useSrefActive("profile", null, activeClass);
  const IMG_PROFILE_API = "https://image.tmdb.org/t/p/w500";
  const [isLoading, setIsLoading] = useState(true);

  function RenameUrl() {
    localStorage.setItem("type", "movie");
  }
  function RenameUrl2() {
    localStorage.setItem("type", "tv");
  }

  useEffect(() => {
    setIsLoading(false);
  }, []);

  return (
    <div className="qqqq">
      {isLoading ? (
        <div>loading</div>
      ) : (
        <header>
          <nav className="navbar navbar-dark bg-dark">
            <a {...homeSref} className="logo__link">
              <img src={logo} alt="logo" />
            </a>
            <div onClick={() => RenameUrl()}>
              <a {...searchSref} className="navbar-brand">
                Movie
              </a>
            </div>
            <div>
              <a {...peopleSref} className="navbar-brand">
                People
              </a>
            </div>
            <div onClick={() => RenameUrl2()}>
              <a {...tvShowSref} className="navbar-brand">
                TV Show
              </a>
            </div>
            <div>
              <a {...homeSref} className="navbar-brand">
                Home
              </a>
            </div>
            {userDate ? (
              <div>
                {localStorage.getItem("username") ? (
                  <a {...profileSref} className="loggedin">
                    {localStorage.getItem("username")}
                  </a>
                ) : null}

                {localStorage.getItem("img") ? (
                  <img
                    className="photo"
                    src={IMG_PROFILE_API + localStorage.getItem("img")}
                    alt="title"
                  />
                ) : null}
              </div>
            ) : null}
          </nav>
        </header>
      )}
    </div>
  );
};
export default observer(Header);
