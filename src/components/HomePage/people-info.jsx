import { React, useState, useEffect, useContext } from "react";
import Movie from "./Movie";
import "../HomePage/peopleInfo.css";
import facebool_icon from "../../img/facebook_icon.png";
import instagram_icon from "../../img/Instagram_icon.png";
import imdb_icon from "../../img/imdb_icon.png";
import twitter_icon from "../../img/twitter_icon.png";
import IdContext from "../../context2";
import Header from "./Header";

const People_info = (props) => {
  const [peopleInfo, setPeopleInfo] = useState({});
  const [external, setExternal] = useState({});
  const [moviePeople, setMoviePeople] = useState([]);
  const [newArr, setNewArr] = useState([]);
  const [use, SetUse] = useState(false);

  const { languageList } = useContext(IdContext);

  const PEOPLEINFO_API = `https://api.themoviedb.org/3/person/${props.$stateParams.id}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`;
  const EXTERNAL_API = `https://api.themoviedb.org/3/person/${props.$stateParams.id}/external_ids?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}`;
  const IMG_API = "https://image.tmdb.org/t/p/w500";
  const MOVIE_PEOPLE_API = `https://api.themoviedb.org/3/person/${props.$stateParams.id}/movie_credits?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}&page=1`;

  useEffect(() => {
    fetch(PEOPLEINFO_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setPeopleInfo(data);
        console.log(peopleInfo);
      });
  }, []);

  useEffect(() => {
    fetch(EXTERNAL_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setExternal(data);
      });
  }, []);

  useEffect(() => {
    fetch(MOVIE_PEOPLE_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setMoviePeople(data.cast);
      });
  }, []);
  useEffect(() => {
    setNewArr(moviePeople.slice(0, 8));
  }, [moviePeople]);

  function ShowAll() {
    setNewArr(moviePeople);
    SetUse(!use);
  }
  function HideAll() {
    setNewArr(moviePeople.slice(0, 8));
    SetUse(!use);
  }

  return (
    <div className="people__container">
      <Header />
      <div className="peple-body">
        <div className="people-img">
          <img
            width={"260px"}
            height={"330px"}
            src={IMG_API + peopleInfo.profile_path}
            alt="photo"
          />
          <ul className="external-container">
            <li>
              <a
                href={`https://www.imdb.com/name/${external.imdb_id}`}
                target="_blank"
              >
                <img width={"35px"} height={"35px"} src={imdb_icon} alt="img" />
              </a>
            </li>
            <li>
              <a
                href={`https://www.instagram.com/${external.instagram_id}`}
                target="_blank"
              >
                <img
                  width={"35px"}
                  height={"35px"}
                  src={instagram_icon}
                  alt="img"
                />
              </a>
            </li>
            <li>
              <a
                href={`https://www.twitter.com/${external.twitter_id}`}
                target="_blank"
              >
                <img
                  width={"35px"}
                  height={"35px"}
                  src={twitter_icon}
                  alt="img"
                />
              </a>
            </li>
            <li>
              <a
                href={`https://www.facebook.com/${external.facebook_id}`}
                target="_blank"
              >
                <img
                  width={"35px"}
                  height={"35px"}
                  src={facebool_icon}
                  alt="img"
                />
              </a>
            </li>
          </ul>
        </div>
        <div>
          <h2 className="people-name">{peopleInfo.name}</h2>
          <div className="info-container">
            <ul>
              <li className="default-info">birthday</li>
              <li className="default-info">popularity</li>
              <li className="default-info">place_of_birth</li>
            </ul>
            <ul>
              <li className="default-info2">{peopleInfo.birthday}</li>
              <li className="default-info2">{peopleInfo.popularity}</li>
              <li className="default-info2">{peopleInfo.place_of_birth}</li>
            </ul>
          </div>
          <p align="justify" className="info-biography">
            {peopleInfo.biography}
          </p>
        </div>
      </div>
      <h2 className="top-movies">Top Movies</h2>
      <div className="people__movie">
        {newArr.length > 0 &&
          newArr.map((movPeop) => <Movie key={movPeop.id} {...movPeop} />)}
      </div>
      <button
        onClick={() => ShowAll()}
        type="button"
        className={`btn btn-primary ${use ? "qaz" : "movieButton"}`}
      >
        Show all
      </button>
      <button
        onClick={() => HideAll()}
        type="button"
        className={`btn btn-danger ${use ? "movieButton" : "qaz"}`}
      >
        Hide movie
      </button>
    </div>
  );
};

export default People_info;
