import { React, useEffect, useState, useContext } from "react";
import { useRouter } from "@uirouter/react";
import "./main-profile.css";
import ProfileMovie from "./ProfileMovie";
import IdContext from "../../context2";
import Movie from "../HomePage/Movie";
import { observer } from "mobx-react-lite";
import "../HomePage/movie.css";
import { Tv } from "react-feather";

const MainProfile = () => {
  const router = useRouter();
  const [profileInfo, setProfileInfo] = useState({});
  const IMG_PROFILE_API = "https://image.tmdb.org/t/p/w500";
  const PROFILE_API = `https://api.themoviedb.org/3/account?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
    "session_id"
  )}`;
  const [isLoading, setIsLoading] = useState(true);
  const [movieInfo, setmovieInfo] = useState([]);
  const [arrRate, setArrRate] = useState();
  const [middleRate, setMiddleRate] = useState();

  const { profilemovieList } = useContext(IdContext);

  useEffect(() => {}, [profileInfo]);

  useEffect(() => {
    setIsLoading(true);
    fetch(
      `https://api.themoviedb.org/3/account?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
        "session_id"
      )}`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProfileInfo(data);
        localStorage.setItem("account_id", data.id);
        localStorage.setItem("username", data.username);
        localStorage.setItem("img", data.avatar.tmdb.avatar_path);
        setIsLoading(false);
      });
  }, []);

  useEffect(() => {
    setIsLoading(true);
    fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/rated/movies?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=en-US&session_id=${localStorage.getItem(
        "session_id"
      )}&sort_by=created_at.asc&page=1`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setmovieInfo(data.results);
        setIsLoading(false);
      });
  }, []);

  useEffect(() => {
    setArrRate(movieInfo.length);
  }, [movieInfo]);

  function LogOut() {
    const result = fetch(
      "https://api.themoviedb.org/3/authentication/session?api_key=5006aee2ee6c14a6a59cfd3b10e5d068",
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "DELETE",
        body: JSON.stringify({
          session_id: localStorage.getItem("session_id"),
        }),
      }
    ).then(() => {
      localStorage.removeItem("session_id");
      localStorage.removeItem("account_id");
      localStorage.removeItem("img");
      localStorage.removeItem("username");
      localStorage.removeItem("request_token");
      router.stateService.go("signIn");
    });
  }

  return (
    <div className="prof-container">
      {isLoading ? (
        <h3>loading</h3>
      ) : (
        <div className="profile__info">
          <img
            className="profile__image"
            src={IMG_PROFILE_API + profileInfo.avatar.tmdb.avatar_path}
            alt="vlad_orpik"
          />
          <h3 className="profile-name">{profileInfo.username}</h3>
          <h3 className="mark-info"> total marks - {arrRate}</h3>
          <button onClick={() => LogOut()} className=" asd2 btn btn-danger">
            log out
          </button>
        </div>
      )}
      <div className="main__info">
        <ProfileMovie />
        <div className="profileMovieContainer">
          {profilemovieList.profilemovieItems
            ? profilemovieList.profilemovieItems.length > 0 &&
              profilemovieList.profilemovieItems.map((movie) => (
                <div>
                  <Movie key={movie.id} {...movie} />
                </div>
              ))
            : null}
        </div>
      </div>
    </div>
  );
};

export default observer(MainProfile);
