import { React, useEffect, useState, useContext } from "react";
import Movie from "../HomePage/Movie";
import ProfileMovieStore from "../../store/profileMovie";
import IdContext from "../../context2";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import "./main-profile.css";
import {
  UIRouter,
  UIView,
  useSrefActive,
  pushStateLocationPlugin,
} from "@uirouter/react";

const ProfileMovie = () => {
  const [generalState, setGeneral] = useState("favorite");

  const { profilemovieList } = useContext(IdContext);

  const activeClass = "active";
  const profile_tvSref = useSrefActive("profile_tv", null, activeClass);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/${generalState}/movies?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=en-US&session_id=${localStorage.getItem(
        "session_id"
      )}&sort_by=created_at.asc&page=1`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        runInAction(() => (profilemovieList.profilemovieItems = data.results));
      });
  }, [generalState]);

  return (
    <div className="tab-items">
      <ul className="nav nav-tabs">
        <li
          onClick={() => {
            setGeneral("watchlist");
          }}
          className="nav-item"
        >
          <a className="text nav-use nav-link " aria-current="page" href="#">
            Get WachList(movie)
          </a>
        </li>
        <li
          onClick={() => {
            setGeneral("rated");
          }}
          className="nav-item"
        >
          <a className="text nav-use nav-link" href="#">
            {" "}
            Get RatedList(movie)
          </a>
        </li>
        <li
          onClick={() => {
            setGeneral("favorite");
          }}
          className="nav-item"
        >
          <a className="text nav-use nav-link" href="#">
            Get FavoriteList(movie)
          </a>
        </li>
        <li
          onClick={() => {
            setGeneral("favorite");
          }}
          className="nav-item"
        >
          <a {...profile_tvSref} className="text nav-use nav-link">
            tv
          </a>
        </li>
      </ul>
    </div>
  );
};

export default observer(ProfileMovie);
