import { React, useEffect, useState, useContext } from "react";
import { useRouter } from "@uirouter/react";
import "./main-profile.css";
import ProfileTv from "./ProfileTv";
import IdContext from "../../context2";
import { observer } from "mobx-react-lite";
import "../HomePage/movie.css";
import Tv from "../TvPage/Tv";

const MainTvProfile = () => {
  const router = useRouter();
  const [profileInfo, setProfileInfo] = useState({});
  const IMG_PROFILE_API = "https://image.tmdb.org/t/p/w500";
  const PROFILE_API = `https://api.themoviedb.org/3/account?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem(
    "session_id"
  )}`;
  const [isLoading, setIsLoading] = useState(true);
  const [movieInfo, setmovieInfo] = useState([]);
  const [arrRate, setArrRate] = useState();

  const { tvList } = useContext(IdContext);

  useEffect(() => {
    fetch(PROFILE_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProfileInfo(data);
        localStorage.setItem("account_id", data.id);
        setIsLoading(false);
        localStorage.setItem("username", data.username);
        localStorage.setItem("img", data.avatar.tmdb.avatar_path);
      });
  }, []);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/account/${localStorage.getItem(
        "account_id"
      )}/rated/tv?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=en-US&session_id=${localStorage.getItem(
        "session_id"
      )}&sort_by=created_at.asc&page=1`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setmovieInfo(data.results);
      });
  }, []);

  useEffect(() => {
    setArrRate(movieInfo.length);
  }, [movieInfo]);

  function LogOut() {
    localStorage.removeItem("session_id");
    localStorage.removeItem("account_id");
    localStorage.removeItem("username");
    localStorage.removeItem("img");
    router.stateService.go("signIn");
  }

  return (
    <div className="prof-container">
      {isLoading ? (
        <h3>loading</h3>
      ) : (
        <div className="profile__info">
          <img
            className="profile__image"
            src={IMG_PROFILE_API + profileInfo.avatar.tmdb.avatar_path}
            alt="vlad_orpik"
          />
          <h3 className="profile-name">{profileInfo.username}</h3>
          <h3 className="mark-info"> total marks - {arrRate}</h3>
          <button onClick={() => LogOut()} className=" asd2 btn btn-danger">
            log out
          </button>
        </div>
      )}
      <div className="main__info">
        <ProfileTv />
        <div className="profileMovieContainer">
          {tvList.tvItem
            ? tvList.tvItem.length > 0 &&
              tvList.tvItem.map((movie) => (
                <div>
                  <Tv qwe="tv" key={movie.id} {...movie} />
                </div>
              ))
            : null}
        </div>
      </div>
    </div>
  );
};

export default observer(MainTvProfile);
