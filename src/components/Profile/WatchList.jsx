import {React,useState,useEffect} from "react";
import './favorite-list.css';
import Movie from "../HomePage/Movie";

const WatchList=()=>{
    const[watchListMovies,setWatchListMovies]=useState([]);
    const[watchId,setwatchId]=useState([]);
    const[isActive,setIsActive]= useState(false)    

    function getWatch(){
        fetch(`https://api.themoviedb.org/3/account/${localStorage.getItem('account_id')}/watchlist/movies?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=en-US&session_id=${localStorage.getItem('session_id')}&sort_by=created_at.asc&page=1`)
        .then((res)=>res.json())
        .then((data)=>{console.log(data)
         setWatchListMovies(data.results)
         setwatchId(data.results)
         console.log(watchId)
        })
        setIsActive(!isActive)
      }
      function HideWatch(){
        setWatchListMovies([])
          setIsActive(!isActive)
      }

    // function deleteItem(id){
    //   fetch(`https://api.themoviedb.org/3/account/${localStorage.getItem("account_id")}/favorite?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem("session_id")}`
    //   ,{
    //    headers: {
    //      'Content-Type': 'application/json'
    //    },
    //    method: 'POST',
    //    body: JSON.stringify(
    //     { "media_type": "movie", "media_id": id, "watchlist": false }
    //    )
    //  }
    //   )
    //   .then((res)=>res.json())
    //   .then((data)=>{console.log(data)
    //     setWatchListMovies(data)
    //   })
    //   .then(()=>fetch(`https://api.themoviedb.org/3/account/${localStorage.getItem('account_id')}/watchlist/movies?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=en-US&session_id=${localStorage.getItem('session_id')}&sort_by=created_at.asc&page=1`)
    //   .then((res)=>res.json())
    //   .then((data)=>{console.log(data)
    //     setWatchListMovies(data.results)
    //    setwatchId(data.results) })
    //   )
    // }

    return(
        <div>
        <button className='getList'onClick={()=>getWatch()}>Get WatchList</button>
        <button className={`${isActive?'getList':'hide'}`} onClick={()=>HideWatch()}>Hide List</button>
        <div className="qwerasd">
        {
            watchListMovies.length>0 && watchListMovies.map((watch)=>
            <div className="card_movie">
            <Movie key={watch.id} {...watch} />
            {/* <button onClick={()=>deleteItem(watch.id)}>Delete Item</button> */}
            </div>
            )
        }
        </div>
    </div>
    )
}

export default WatchList;