import { useRouter } from "@uirouter/react";
import { React, useContext, useEffect, useState } from "react";
import "./SignIn-form.css";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import IdContext from "../../context2";
import UserStore from "../../store/userState";
import { observer } from "mobx-react-lite";
import {
  UIRouter,
  UIView,
  useSrefActive,
  pushStateLocationPlugin,
} from "@uirouter/react";

function SignIn_Form() {
  const [userName, setUserName] = useState(undefined);
  const [passwordH, setPasswordH] = useState(undefined);
  const [data, setData] = useState(undefined);
  const [token, setToken] = useState();

  const { userDate } = useContext(IdContext);

  const router = useRouter();

  async function getToken() {
    try {
      const res = await fetch(
        "https://api.themoviedb.org/3/authentication/token/new?api_key=5006aee2ee6c14a6a59cfd3b10e5d068"
      );
      //  res.json().then(data=>setToken(data.request_token))
      res.json().then((data) => {
        localStorage.setItem("request_token", data.request_token);
        setToken(localStorage.getItem("request_token"));
      });
    } catch (err) {
      alert(err);
    }
  }
  useEffect(() => {
    if (token)
      window.location.href = `https://www.themoviedb.org/authenticate/${token}?redirect_to=http://localhost:3000`;
    console.log(token);
  }, [token]);

  useEffect(() => {
    const url = new URL(window.location.href);
    if (
      url.searchParams.get("approved") &&
      url.searchParams.get("approved") == true
    ) {
      // setToken(url.searchParams.get('request_token'))
    }
    console.log(url.searchParams.get("approved"));
    // setToken(url.searchParams.get('request_token'))
  }, []);

  async function sendRequest(e) {
    try {
      const result = await fetch(
        "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=5006aee2ee6c14a6a59cfd3b10e5d068",
        {
          headers: {
            "Content-Type": "application/json",
          },
          method: "POST",
          body: JSON.stringify({
            username: userName,
            password: passwordH,
            request_token: localStorage.getItem("request_token"),
          }),
        }
      );

      if (`${result.status}`[0] == "4") {
        const resultOb = await result.json();
        alert(resultOb.status_message);
      }

      result.json().then((data) => {
        setData(data);
        userDate.userItem = data.username;
      });
    } catch (error) {
      alert("asdasd");
    }
  }

  useEffect(() => {
    try {
      if (data && data.success == true) {
        const result = fetch(
          "https://api.themoviedb.org/3/authentication/session/new?api_key=5006aee2ee6c14a6a59cfd3b10e5d068",
          {
            headers: {
              "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify({
              request_token: localStorage.getItem("request_token"),
            }),
          }
        ).then((result) => {
          if (result.status[0] == "4") {
            // throw Error(result.statusText);
            console.log(result.status);
          }
          return result
            .json()
            .then((data) => localStorage.setItem("session_id", data.session_id))
            .then(() => router.stateService.go("profile"))
            .catch((err) => {
              alert(err);
            });
        });
      }
      console.log(data);
    } catch (err) {
      alert(err);
    }
  }, [data]);

  const validate = Yup.object({
    username: Yup.string()
      .max(15, "Must be 15 characters or less")
      .required("Required"),
    password: Yup.string()
      .min(6, "Password must be at least 6 charaters")
      .required("Password is required"),
  });

  return (
    <div className="sign_in">
      <Formik
        initialValues={{
          email: "",
          //  password:'',
          confirmPassword: "",
        }}
        validationSchema={validate}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {(formik) => (
          <div className="rty">
            <h1 className=" sign-title my-4 font-weight-bold-display-4">
              Sign In
            </h1>
            {console.log(formik.values)}
            <Form>
              <div>
                <div className="form-group">
                  <label htmlFor="exampleInputUsername1">Username</label>
                  <input
                    type="text"
                    name="username"
                    className="form-control"
                    id="exampleInputUserName"
                    aria-describedby="usernameHelp"
                    placeholder="Enter username"
                    onChange={(e) => setUserName(e.target.value)}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Password</label>
                  <input
                    type="password"
                    name="password"
                    className="form-control"
                    id="exampleInputPassword1"
                    placeholder="Password"
                    onChange={(e) => setPasswordH(e.target.value)}
                  />
                </div>
                <div className="req_input">
                  <input
                    type="text"
                    name="request_token"
                    className="form-control"
                    value={localStorage.getItem("request_token")}
                  />
                </div>
                <div className="boutton-con">
                  <button
                    onClick={() => sendRequest()}
                    UISref
                    className="btn btn-primary mt-4 btn-sm"
                    type="submit"
                  >
                    Register
                  </button>
                  <button className=" btn btn-danger mt-4 btn-sm " type="reset">
                    Reset
                  </button>
                  <button
                    onClick={() => getToken()}
                    className="btn btn-success mt-4 btn-sm "
                    type="reset"
                  >
                    Get request tocken
                  </button>
                  <a
                    href="https://www.themoviedb.org/signup?language=ru"
                    className="sign-link"
                    target="blank"
                  >
                    No account?
                  </a>
                </div>
                {/* <TextField label ="Username" name="username" type="text"  onChange={(e)=>setUserName(e.target.value)}/>
                 <TextField label ="Password" name="password" type="password"  onChange={(e)=>setPasswordH(e.target.value)} />
                 <TextField className="req_input"  name="request_token"  type="text" value={localStorage.getItem("request_token")} /> */}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </div>
  );
}

export default observer(SignIn_Form);
