import { React, useState, useEffect, useContext, useRef } from "react";
import IdContext from "../../context2";
import { observer } from "mobx-react-lite";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { runInAction } from "mobx";
import "./search-movie.css";
import MultiRangeSlider from "./MultiRange";

const SearchByYear = () => {
  const [release_year, setReleaseyear] = useState("");
  const [air, setAir] = useState("");
  const { languageList } = useContext(IdContext);
  const [currentPage, setCurrentPage] = useState(1);
  const [fetching, setFetching] = useState(true);
  const [genresId, setGenresId] = useState("");
  const [sort, setSort] = useState("popularity.desc");
  const { movieList } = useContext(IdContext);
  const [genres, setGenres] = useState([]);
  const [minValue, setMinValue] = useState();
  const [maxValue, setMaxValue] = useState();
  const [fromDate, setFromDate] = useState();
  const [beforeDate, setBeforeDate] = useState();
  const [fromAirDate, setFromAirDate] = useState();
  const [beforeAirDate, setBeforeAirDate] = useState();
  const [originalLanguage, setOrinalLanguage] = useState(
    "&with_original_language=en"
  );

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/genre/${localStorage.getItem(
        "type"
      )}/list?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${
        languageList.languageItem
      }`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        console.log(genres);
        setGenres(data.genres);
      });
  }, []);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/discover/${localStorage.getItem(
        "type"
      )}?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${
        languageList.languageItem
      }&sort_by=${sort}&page=${currentPage}${release_year}${genresId}${maxValue}${minValue}${fromDate}${beforeDate}${fromAirDate}${beforeAirDate}${air}${originalLanguage}`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        // setCurrentPage((prevState) => prevState + 1);
        runInAction(() => (movieList.movieItems = data.results));
      });
    // .finally(() => setFetching(false));
  }, [
    release_year,
    genresId,
    sort,
    maxValue,
    minValue,
    beforeDate,
    air,
    originalLanguage,
    beforeAirDate,
  ]);

  useEffect(() => {
    document.addEventListener("scroll", scrollHandler);
    return function () {
      document.removeEventListener("scroll", scrollHandler);
    };
  }, []);

  const scrollHandler = (e) => {
    if (
      e.target.documentElement.scrollHeight -
        (e.target.documentElement.scrollTop + window.innerHeight) <
      100
    ) {
      setFetching(true);
    }
  };

  function Send({ min, max }) {
    zxc({ min, max });
  }
  function zxc({ max, min }) {
    setMaxValue(`&with_runtime.lte=${max}`);
    setMinValue(`&with_runtime.gte=${min}`);
  }

  return (
    <div>
      <div className="genres-con">
        {genres.length > 0 &&
          genres.map((gen) => {
            return (
              <button
                onClick={() => setGenresId(`&with_genres=${gen.id}`)}
                type="button"
                className=" tab btn btn-warning"
              >
                {gen.name}
              </button>
            );
          })}
      </div>
      <div className="filter-con">
        <div className="select-con">
          <select
            onChange={(e) => setSort(e.target.value)}
            class="form-select"
            aria-label="Default select example"
          >
            <option selected disabled>
              Sort by
            </option>
            <option value="popularity.desc">Popularity (desc)</option>
            <option value="popularity.asc">Popularity (asc)</option>
            <option value="release_date.asc">Release date (asc)</option>
            <option value="release_date.desc">Release date (desc)</option>
            <option value="release_date.desc">Release date (desc)</option>
            <option value="revenue.asc">Revenue (asc)</option>
            <option value="revenue.desc">Revenue (desc)</option>
            <option value="original_title.asc">Original title (asc)</option>
            <option value="original_title.desc">Original title (desc)</option>
            <option value="vote_count.asc">Vote count (asc)</option>
            <option value="vote_count.desc">Vote count (desc)</option>
            <option value="vote_average.asc">Vote average (asc)</option>
            <option value="vote_average.desc">Vote average (desc)</option>
          </select>
        </div>
        <div>
          <label className="input-label" for="site-search">
            Search by year
          </label>
          <input
            type="search"
            id="site-search"
            placeholder="year"
            aria-label="Search"
            // min="1874"
            // max={new Date().getFullYear()}
            // step="1"
            onChange={(e) => {
              setReleaseyear(`&primary_release_year=${e.target.value}`);
              setAir(`&first_air_date_year=${e.target.value}`);
            }}
          />
        </div>
        <div>
          <input
            className="data-in"
            type="date"
            onChange={(e) => {
              setFromDate(`&release_date.gte=${e.target.value}`);
              setFromAirDate(`&first_air_date.gte=${e.target.value}`);
            }}
          />
          <input
            type="date"
            onChange={(e) => {
              setBeforeDate(`&release_date.lte=${e.target.value}`);
              setBeforeAirDate(`&first_air_date.lte=${e.target.value}`);
            }}
          />
        </div>
        <div className="multiRange">
          <MultiRangeSlider
            min={0}
            max={400}
            onChange={({ min, max }) => Send({ min, max })}
          />
          {/* <div>
            <button onClick={({ min, max }) => Send({ min, max })}>Send</button>
          </div> */}
        </div>
        <div className="select-con2">
          <select
            onChange={(e) => setOrinalLanguage(e.target.value)}
            class="form-select"
            aria-label="Default select example"
          >
            <option selected disabled>
              Сountry
            </option>
            <option value={"&with_original_language=ru"}>Russian</option>
            <option value={"&with_original_language=en"}>English</option>
            <option value={"&with_original_language=pl"}>Polish</option>
            <option value={"&with_original_language=fr"}>French</option>
            <option value={"&with_original_language=de"}>German</option>
          </select>
        </div>
      </div>
    </div>
  );
};

export default observer(SearchByYear);
