import {
  UIRouter, UIView, useSrefActive, pushStateLocationPlugin,
} from '@uirouter/react';
import './App.css';
import Home from './pages/Home';
import {React,useEffect} from 'react';
import UserStore from './store/userState';
import IdContext from './context2';
import { useContext } from 'react';
import { observer } from 'mobx-react-lite';

function App() {
    const{userDate}=useContext(IdContext)

  useEffect(()=>{
    if(localStorage.getItem('session_id')){
      fetch(`https://api.themoviedb.org/3/account?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&session_id=${localStorage.getItem('session_id')}`)
      .then((res)=>res.json())
      .then((data)=>{console.log(data)
        userDate.userItem=data.username
      })
      
    }
},[])
  return (
    <div className="App">
      
      <UIView />
    </div>
  );
}

export default observer(App);
