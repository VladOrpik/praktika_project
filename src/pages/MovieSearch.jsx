import { React, useState, useEffect, useContext } from "react";
import Movie from "../components/HomePage/Movie";
import SearchByYear from "../components/Search/SearchByYear";
import { observer } from "mobx-react-lite";
import IdContext from "../context2";
import "../components/Search/search-movie.css";
import Header from "../components/HomePage/Header";

const MovieSearch = () => {
  const { movieList } = useContext(IdContext);
  const { languageList } = useContext(IdContext);
  const [searchTerm, setSearchTerm] = useState("");

  const SEARCHAPI = `https://api.themoviedb.org/3/search/movie?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}&query=${searchTerm}&page=1&include_adult=false`;

  const handleOnSumbit = (e) => {
    e.preventDefault();
    if (searchTerm) {
      fetch(SEARCHAPI)
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          movieList.movieItems = data.results;
        });
      setSearchTerm("");
    }
  };
  const handleOnChange = (e) => {
    setSearchTerm(e.target.value);
  };

  return (
    <div className="movieSearch-container">
      <Header />
      <div className="search-movie__input">
        <h2 className="search-title">Movies</h2>
        <form
          onSubmit={handleOnSumbit}
          className=" d-flex form-inline my-2 my-lg-0"
        >
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
            value={searchTerm}
            onChange={handleOnChange}
          />
        </form>
      </div>
      <SearchByYear />
      <div className="movie-container">
        {movieList.movieItems.length > 0 &&
          movieList.movieItems.map((movie) => (
            <div>
              <Movie key={movie.id} {...movie} />
            </div>
          ))}
      </div>
    </div>
  );
};

export default observer(MovieSearch);
