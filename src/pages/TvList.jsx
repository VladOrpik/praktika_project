import React, { useContext, useEffect, useState } from "react";
import {
  UIRouter,
  UIView,
  useSrefActive,
  pushStateLocationPlugin,
} from "@uirouter/react";
import Header from "../components/HomePage/Header";
import MainTvProfile from "../components/Profile/MainTvProfile";
import { observer } from "mobx-react-lite";

const TvList = () => {
  return (
    <div>
      <Header />
      <MainTvProfile />
    </div>
  );
};

export default observer(TvList);
