import { UIRouter, UIView, useSrefActive, pushStateLocationPlugin } from "@uirouter/react";
import Signup_Form from "../components/SignUp/Signup_Form";
import '../components/SignUp/Signup-form.css'
import ramka from '../img/ramka.jpg';

function SignUp() {
    return (
      <div className='container mt-3'>
        <div className='col-md-5'>
            <Signup_Form/>
        </div>
        </div>
    );
  }
  
  export default SignUp;