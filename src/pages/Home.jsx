import React, { useContext, useEffect, useState } from "react";
import Movie from "../components/HomePage/Movie";
import Tv from "../components/TvPage/Tv.js";
import People from "../components/HomePage/People";
import BootstrapCarousel from "../components/HomePage/BootstrapCarousel";
import "../components/HomePage/movie.css";
import logo from "../img/Logo.png";
import "../components/HomePage/header.css";
import {
  UIRouter,
  UIView,
  useSrefActive,
  pushStateLocationPlugin,
} from "@uirouter/react";
import { observer } from "mobx-react-lite";
import IdContext from "../context2";

function Home() {
  const { userDate } = useContext(IdContext);
  const { languageList } = useContext(IdContext);
  const { urlList } = useContext(IdContext);

  const IMG_PROFILE_API = "https://image.tmdb.org/t/p/w500";

  const [movies, setMovies] = useState([]);
  const [moviesNewArr, setMoviesNewArr] = useState([]);
  const [peoples, setPeoples] = useState([]);
  const [peoplesNewArr, setPeoplesNewArr] = useState([]);
  const [tv, setTv] = useState([]);
  const [tvNewArr, setTvNewArr] = useState([]);

  const activeClass = "active";
  const signInSref = useSrefActive("signIn", null, activeClass);
  const searchSref = useSrefActive("search", null, activeClass);
  const signUpSref = useSrefActive("signUp", null, activeClass);
  const peopleSref = useSrefActive("peopleSearch", null, activeClass);
  const tvShowSref = useSrefActive("tv_show", null, activeClass);
  const profileSref = useSrefActive("profile", null, activeClass);
  const playerSref = useSrefActive("movie_player", null, activeClass);
  const [isLoading, setIsLoading] = useState(true);

  const [language, setLanguage] = useState("en");

  let FEATURED_API = `https://api.themoviedb.org/3/trending/movie/day?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${language}&page=1`;
  const CAROUSEL_API = `https://api.themoviedb.org/3/movie/now_playing?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${language}&page=1`;
  let PEOPLE_API = `https://api.themoviedb.org/3/trending/person/day?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${language}&page=1`;
  const TV_API = `https://api.themoviedb.org/3/trending/tv/day?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${language}&page=1`;

  useEffect(() => {
    fetch(FEATURED_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setMovies(data.results);
        setIsLoading(false);
      });
    languageList.languageItem = language;
    console.log(languageList.languageItem);
  }, [language]);

  useEffect(() => {
    setMoviesNewArr(movies.slice(0, 8));
    setPeoplesNewArr(peoples.slice(0, 8));
    setTvNewArr(tv.slice(0, 8));
  }, [movies, peoples, tv]);

  useEffect(() => {
    fetch(PEOPLE_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setPeoples(data.results);
        setIsLoading(false);
      });
  }, [language]);

  useEffect(() => {
    fetch(TV_API)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setTv(data.results);
        setIsLoading(false);
      });
  }, [language]);

  function RenameUrl() {
    localStorage.setItem("type", "movie");
  }
  function RenameUrl2() {
    localStorage.setItem("type", "tv");
  }

  return (
    <div className="Home">
      {isLoading ? (
        <div>loading</div>
      ) : (
        <div className="header">
          <header>
            <nav className="navbar navbar-dark bg-dark">
              <a className="logo__link" href="#">
                <img src={logo} alt="logo" />
              </a>
              <div onClick={() => RenameUrl()}>
                <a {...searchSref} className="navbar-brand">
                  Movie
                </a>
              </div>
              <div onClick={() => RenameUrl2()}>
                <a {...tvShowSref} className="navbar-brand" href="#">
                  TV Show
                </a>
              </div>
              <div>
                <a {...peopleSref} className="navbar-brand">
                  People
                </a>
              </div>
              <div>
                <a {...playerSref} className="navbar-brand">
                  Player
                </a>
              </div>
              {userDate ? (
                <div>
                  <a {...profileSref} className="loggedin">
                    profile:{localStorage.getItem("username")}
                  </a>
                  <img
                    className="photo"
                    src={IMG_PROFILE_API + localStorage.getItem("img")}
                    alt="title"
                  />
                </div>
              ) : (
                <div className="register-link">
                  <a {...signInSref} className=" navbar-brand reg__link">
                    Sign In
                  </a>
                  <a
                    href="https://www.themoviedb.org/signup?language=ru"
                    target="_blank"
                    className="navbar-brand"
                    rel="noreferrer"
                  >
                    Sign Up
                  </a>
                </div>
              )}

              <div
                className="btn-group"
                role="group"
                aria-label="Basic example"
              >
                <button
                  type="button"
                  onClick={() => setLanguage("ru")}
                  className="btn btn-primary"
                >
                  ru
                </button>
                <button
                  type="button"
                  onClick={() => setLanguage("en")}
                  className="btn btn-primary"
                >
                  en
                </button>
                <button
                  type="button"
                  onClick={() => setLanguage("de")}
                  className="btn btn-primary"
                >
                  de
                </button>
              </div>
            </nav>
          </header>
        </div>
      )}
      {isLoading ? (
        <div>loading</div>
      ) : (
        <div>
          <div className="bootstrapCarousel">
            <BootstrapCarousel id={movies.id} {...movies} />
          </div>
          <h2 className="trend-title">popular movies of the day</h2>
          <hr className="hr-home" />
          <div className="movie-container">
            {moviesNewArr.length > 0 &&
              moviesNewArr.map((movie) => (
                <div key={movie.id}>
                  <Movie {...movie} />
                </div>
              ))}
          </div>
          <h2 className="trend-title">popular tv shows of the day</h2>
          <hr className="hr-home" />
          <div className=" movie-container">
            {tvNewArr.length > 0 &&
              tvNewArr.map((tvInfo) => (
                <div key={tvInfo.id}>
                  <Tv {...tvInfo} qwe="tv" />
                </div>
              ))}
          </div>
          <h2 className="trend-title">popular actors of the day</h2>
          <hr className="hr-home" />
          <div className="people-container">
            {peoplesNewArr.length > 0 &&
              peoplesNewArr.map((peop) => (
                <div key={peop.id}>
                  <People {...peop} />
                </div>
              ))}
          </div>
        </div>
      )}
    </div>
  );
}

export default observer(Home);
