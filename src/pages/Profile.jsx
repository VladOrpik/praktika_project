import { useState, useEffect, useContext } from "react";
import React from "react";
import "../components/profile.css";
import "../components/HomePage/movie.css";
import { Card, ListGroupItem, ListGroup } from "react-bootstrap";
import MainProfile from "../components/Profile/MainProfile";
import IdContext from "../context2";
import { observer } from "mobx-react-lite";
import HeaderProfile from "../components/HomePage/HeaderProfile";
import Header from "../components/HomePage/Header";

const Profile = () => {
  return (
    <div className="profile">
      <HeaderProfile />
      <MainProfile />
    </div>
  );
};

export default observer(Profile);
