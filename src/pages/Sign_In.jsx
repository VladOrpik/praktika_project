import {
  UIRouter,
  UIView,
  useSrefActive,
  pushStateLocationPlugin,
} from "@uirouter/react";
import SignIn_Form from "../components/SignIn/SignIn_Form";
import "../components/SignIn/SignIn-form.css";

function SignIn() {
  return (
    <div className="container-sigIn mt-8">
      <div className="col-md-5 сenter-block">
        <SignIn_Form />
      </div>
    </div>
  );
}

export default SignIn;
