import React, { useEffect, useState, useContext } from "react";
import "../components/TvPage/tv-show.css";
import IdContext from "../context2";
import SearchTv from "../components/Search/SearchTv";
import Tv from "../components/TvPage/Tv";
import "../components/Search/search-movie.css";
import { observer } from "mobx-react-lite";
import Header from "../components/HomePage/Header";
import SearchByYear from "../components/Search/SearchByYear";

function Tv_Show() {
  const { movieList } = useContext(IdContext);
  const { languageList } = useContext(IdContext);
  const [searchTerm, setSearchTerm] = useState("");
  const { tvList } = useContext(IdContext);
  const TV_SEARCH = `https://api.themoviedb.org/3/search/tv?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=${languageList.languageItem}&page=1&query=${searchTerm}&include_adult=false`;

  const handleOnSumbit = (e) => {
    e.preventDefault();
    if (searchTerm) {
      fetch(TV_SEARCH)
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          movieList.movieItems = data.results;
        });
      setSearchTerm("");
    }
  };
  const handleOnChange = (e) => {
    setSearchTerm(e.target.value);
  };

  return (
    <div className="movieSearch-container">
      <Header />
      <div className="search-movie__input">
        <h2 className="search-title">TV_SHOWS</h2>
        <form
          onSubmit={handleOnSumbit}
          className=" d-flex form-inline my-2 my-lg-0"
        >
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
            value={searchTerm}
            onChange={handleOnChange}
          />
        </form>
      </div>
      <SearchByYear />
      <div className="movie-container">
        {movieList.movieItems.length > 0 &&
          movieList.movieItems.map((movie) => (
            <div>
              <Tv qwe="tv" key={movie.id} {...movie} />
            </div>
          ))}
      </div>
    </div>
  );
}

export default observer(Tv_Show);
