import React, { useContext, useEffect, useState } from "react";
import { Helmet } from "react-helmet";
const MoviePlayer = () => {
  return (
    <div>
      <Helmet>
        <script src="https://kinobd.ru/js/player_.js"></script>
      </Helmet>
      <div data-kinopoisk="435" id="kinobd"></div>
    </div>
  );
};

export default MoviePlayer;
