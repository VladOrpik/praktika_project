import React, { useEffect, useState } from "react";
import People from "../components/HomePage/People";
import Header from "../components/HomePage/Header";
import "./people-search.css";

const PeopleSearch = () => {
  const [popular, setPopular] = useState([]);

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/person/popular?api_key=5006aee2ee6c14a6a59cfd3b10e5d068&language=en-US&page=1`
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setPopular(data.results);
      });
  }, []);

  return (
    <div className="search-con">
      <Header />
      <div>
        {/* <input
          className="form-control mr-sm-2"
          type="search"
          placeholder="Search"
          aria-label="Search"
        /> */}
      </div>
      <h3 className="people-title">Popular People</h3>
      <div className="people-con">
        {popular.length > 0 &&
          popular.map((people) => {
            return <People key={people.id} {...people} />;
          })}
      </div>
    </div>
  );
};

export default PeopleSearch;
