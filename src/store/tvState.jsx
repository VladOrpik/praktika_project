import { makeAutoObservable } from "mobx";

class TVStore {
  constructor() {
    this.tvItem = [];
    makeAutoObservable(this);
  }
}

export default TVStore;
