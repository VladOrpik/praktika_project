import {makeAutoObservable} from 'mobx'

class UserStore{
    constructor(){
        this.userItems={};
        makeAutoObservable(this)
    }
}


export default UserStore;