import {makeAutoObservable} from 'mobx'

class MovieStore{
    constructor(){
        this.movieItems=[];
        makeAutoObservable(this)
    }
}


export default MovieStore;