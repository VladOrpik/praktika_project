import {makeAutoObservable} from 'mobx'

class ProfileMovieStore{
    constructor(){
        this.profilemovieItems=[];
        makeAutoObservable(this)
    }
}


export default ProfileMovieStore;