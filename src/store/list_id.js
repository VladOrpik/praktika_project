import {makeAutoObservable} from 'mobx'

class ListStore{
    constructor(){
        this.id=null;
        makeAutoObservable(this)
    }
}


export default ListStore;