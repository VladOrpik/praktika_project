import { makeAutoObservable } from "mobx";

class SeasonStore {
  constructor() {
    this.seasonItem = null;
    makeAutoObservable(this);
  }
}

export default SeasonStore;
