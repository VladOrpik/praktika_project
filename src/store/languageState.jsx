import { makeAutoObservable } from "mobx";

class LanguageStore {
  constructor() {
    this.languageItem = null;
    makeAutoObservable(this);
  }
}

export default LanguageStore;
