import { makeAutoObservable } from "mobx";

class RateStore {
  constructor() {
    this.rateItems = null;
    makeAutoObservable(this);
  }
}

export default RateStore;
