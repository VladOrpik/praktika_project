import React from 'react';
import ReactDOM from 'react-dom';
import { UIRouter, UIView, useSrefActive, pushStateLocationPlugin } from "@uirouter/react";
import './index.css';
import App from './App';
import Home from './pages/Home.jsx'
import SignIn from './pages/Sign_In';
import SignUp from './pages/Sign_Up';
import Tv_show from './pages/Tv_Show';
import 'bootstrap/dist/css/bootstrap.min.css'
import Profile  from './pages/Profile.jsx' 
import ListStore from './store/list_id';
import IdContext from './context2';
import Movie_info from './components/HomePage/Movie_info';
import People_info from './components/HomePage/people-info';
import MovieSearch from './pages/MovieSearch';
import MovieStore from './store/moviesState';
import UserStore from './store/userState';
import RateStore from './store/rateState';
import TvStore from './store/tvState';
import ProfileMovieStore from './store/profileMovie';
import LanguageStore from './store/languageState';
import SeasonStore from './store/seasonNumber';
import Tv_info from './components/TvPage/TvInfoCompanents/Tv_info';
import Seasons_info from './components/TvPage/TvInfoCompanents/TvSeasonsCompanents/Season_info';
import TvEpisods from './components/TvPage/TvInfoCompanents/TvEpisods/TvEpisods';
import PeopleSearch from './pages/PeopleSearch';
import TvList from './pages/TvList';
import MoviePlayer from './pages/MoviePlayer';

const home = { name: 'home', url: '/home', component: Home };
const peopleSearch = { name: 'peopleSearch', url: '/peopleSearch', component: PeopleSearch };
const signIn = { name: 'signIn', url: '/', component: SignIn };
const signUp = { name: 'signUp', url: '/signUp', component: SignUp};
const tv_show = { name: 'tv_show', url: '/tv_show', component: Tv_show};
const movie_player = { name: 'movie_player', url: '/movie_player', component: MoviePlayer};
const profile = { name: 'profile', url: '/profile', component: Profile};
const movie_info = { name: 'movie', url: '/movie_info/:id', component: Movie_info};
const tv_info = { name: 'tv', url: '/tv_info/:id', component: Tv_info };
const profile_tv = { name: 'profile_tv', url: '/tv_list', component: TvList};
const tv_season = { name: 'tv_season', url: '/tv/:id/season/:season_number', component: Seasons_info};
const tv_episod = { name: 'tv_episod', url: '/tv/:id/season/:season_number/episode/:episode_number', component: TvEpisods};

const people_info = { name: 'people', url: 'people_info/:id', component: People_info};
const search = {name: 'search', url: '/movie', component: MovieSearch}

ReactDOM.render(
    <IdContext.Provider value={{profileList:new ListStore(),movieList:new MovieStore(),languageList:new LanguageStore(),seasonList:new SeasonStore(),tvList:new TvStore(),profilemovieList:new ProfileMovieStore(),userDate:new UserStore(),rateList:new RateStore()}}>
  <UIRouter
  plugins={[pushStateLocationPlugin]} 
  states={[home,signIn,signUp,tv_show,profile,movie_info,tv_info,search,people_info,tv_season,tv_episod,peopleSearch,profile_tv,movie_player]}>
    <App />
    </UIRouter>
    </IdContext.Provider>,
  document.getElementById('root')
);

